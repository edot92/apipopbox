import DashView from './components/Dash.vue'
// import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'
import Buttonn from './components/views/popbox/Button'
import Proses from './components/views/popbox/Proses'
// import Kurirambil from './components/views/popbox/Kurirambil'
// import Kurirsimpan from './components/views/popbox/Kurirsimpan'
import Customersimpan from './components/views/popbox/Customersimpan'
import Customerambil from './components/views/popbox/Customerambil'

// Routes
const routes = [

  //
  // {
  //   path: '/login',
  //   component: LoginView
  // },
  {
    path: '/',
    component: DashView,
    children: [

      {
        path: 'button',
        component: Buttonn,
        name: 'Demo Test Control Locker ',
        meta: { description: '' }
      },
      {
        path: 'proses',
        component: Proses,
        name: 'Demo Kirim Barang',
        meta: { description: '' }
      },
      // {
      //   path: 'kurirambil',
      //   component: Kurirambil,
      //   name: 'Demo Kurir Ambil Barang',
      //   meta: { description: '' }
      // },
      // {
      //   path: 'kurirsimpan',
      //   component: Kurirsimpan,
      //   name: 'Demo Kurir Taruh Barang',
      //   meta: { description: '' }
      // },
      {
        path: 'customersimpan',
        component: Customersimpan,
        name: 'Demo Simpan Barang',
        meta: { description: '' }
      },
      {
        path: 'customerambil',
        component: Customerambil,
        name: 'Demo Ambil Barang',
        meta: { description: '' }
      }

      // {
      //   path: 'inputtahananbaru',
      //   component: Inputtahananbaru,
      //   name: 'Inputtahananbaru',
      //   meta: { description: 'Overview of Inputtahananbaru' }
      // },
      // {
      //   path: 'potongremisitahanan',
      //   component: Remisitahanan,
      //   name: 'Potongremisitahanan',
      //   meta: { description: 'Overview of Remisitahanan' }
      // },
      // {
      //   path: 'pencariantahanan',
      //   component: Pencariantahanan,
      //   name: 'Pencariantahanan',
      //   meta: { description: 'Overview of Pencariantahanan' }
      // },
      // {
      //   path: 'settingreminder',
      //   component: Settingreminder,
      //   name: 'Settingreminder',
      //   meta: { description: 'Overview of Pencariantahanan' }
      // },
      // {
      //   path: 'dashboard',
      //   alias: '',
      //   component: DashboardView,
      //   name: 'Dashboard',
      //   meta: {description: 'Overview of environment'}
      // }, {
      //   path: 'tables',
      //   component: TablesView,
      //   name: 'Tables',
      //   meta: {description: 'Simple and advance table in CoPilot'}
      // }, {
      //   path: 'tasks',
      //   component: TasksView,
      //   name: 'Tasks',
      //   meta: {description: 'Tasks page in the form of a timeline'}
      // }, {
      //   path: 'setting',
      //   component: SettingView,
      //   name: 'Settings',
      //   meta: {description: 'User settings page'}
      // }, {
      //   path: 'access',
      //   component: AccessView,
      //   name: 'Access',
      //   meta: {description: 'Example of using maps'}
      // }, {
      //   path: 'server',
      //   component: ServerView,
      //   name: 'Servers',
      //   meta: {description: 'List of our servers'}
      // }, {
      //   path: 'repos',
      //   component: ReposView,
      //   name: 'Repository',
      //   meta: {description: 'List of popular javascript repos'}
      // }
    ]
  }, {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes
