#simple build to all platform and copy conf :)
#for linux you must set permision to excecute
all: build

build: app

app:
	#env  GOOS=darwin GOARCH=amd64 go build  -o ./build/darwin_amd64/apipopbox
	#cp -f -R  conf build/darwin_amd64 
	#cp -f -R  swagger build/darwin_amd64 
	#env  GOOS=linux GOARCH=386 go build  -o ./build/linux_386/apipopbox
	#cp -f -R  conf build/linux_386 
	#cp -f -R swagger build/linux_386
	#env  GOOS=linux GOARCH=amd64 go build  -o  ./build/linux_amd64/apipopbox
	#cp -f -R conf build/linux_amd64
	#cp -f -R swagger build/linux_amd64
	env  GOOS=linux GOARCH=arm go build  -o ./build/linux_arm/apipopbox
	cp -f -R conf build/linux_arm
	cp -f -R swagger build/linux_arm
	cp -f -R webui build/linux_arm
	#env  GOOS=windows GOARCH=386 go build  -o ./build/windows_386/apipopbox.exe
	#cp -f -R conf build/windows_386
	#cp -f -R swagger build/windows_386
	#env  GOOS=windows GOARCH=amd64 go build  -o ./build/windows_amd64/apipopbox.exe
	#cp -f -R conf build/windows_amd64
	#cp -f -R swagger build/windows_amd64
	#env  GOOS=linux GOARCH=mips64 go build  -o ./build/linux_mips64/apipopbox
	#cp -f -R conf build/linux_mips64
	#cp -f -R swagger build/linux_mips64
	#env  GOOS=linux GOARCH=mips64le go build  -o ./build/linux_mips64le/apipopbox
	#cp -f -R conf build/linux_mips64le
	#cp -f -R swagger build/linux_mips64le
	#env  GOOS=linux GOARCH=mips go build  -o ./build/linux_mips/apipopbox
	#cp -f -R conf build/linux_mips
	#cp -f -R swagger build/linux_mips
	#env  GOOS=linux GOARCH=mipsle go build  -o ./build/linux_mipsle/apipopbox
	#cp -f -R conf build/linux_mipsle
	#cp -f -R swagger build/linux_mipsle
	