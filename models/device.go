package models

type ResponseJSONOk struct {
	Message string `json:"message"`
}
type ResponseJSONBad struct {
	Code  string `json:"code"`
	Error string `json:"error"`
}

// Paramdevice json body to force on selenoid board
type Paramdevice struct {
	BoardVersion string `json:"board_version"`
	NoBoard      string `json:"no_board"`
	NoChannel    string `json:"no_channel"`
}

type ParamBoardProperties struct {
	BoardVersion string `json:"board_version"`
}
type ResponBoardProperties struct {
	BoardVersion string `json:"board_version"`
	MaxChannel   string `json:"max_channel"`
	MaxBoardSlot string `json:"max_board_slot"`
}

type ParamPopsend struct {
	Token        string `json:"token"`
	InvoiceID    string `json:"invoice_id"`
	LockerName   string `json:"locker_name"`
	LockerSize   string `json:"locker_size"`
	LockerNumber string `json:"locker_number"`
}
