package controllers

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/edot92/apipopbox/engine"
	"gitlab.com/edot92/apipopbox/models"

	"github.com/asaskevich/govalidator"
	"github.com/astaxie/beego"
)

// var listDevice []int{
// 	0,1,2,
// }

// func init() {

// }

// DeviceController Operations to operate locker
type DeviceController struct {
	beego.Controller
}

// Cmd function api to open locker
// @Title Command Locker
// @Description force on popbox channel selenoid based on board version
// @Param	body	body 	models.Paramdevice	true		"param locker number and board version to open"
// @Success 200 {object} models.device.ResponseJSONOk  "succes open locker"
// @Failure 400 "{"Code": "1", "Error": "param invalid"} ,{"Code": "2", "Error": "please set max locker value on folder conf/app.conf"},{"Code": "3", "Error": "your set locker is more than available locker door or value locker must > 0"}"
// @router /forceon [post]
func (c *DeviceController) Forceon() {
	var ob models.Paramdevice
	var resBad models.ResponseJSONBad
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &ob)
	if err != nil {
		resBad.Error = "param invalid"
		resBad.Code = "1"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		fmt.Println(resBad)
		return
	}
	// maxLocker, err := beego.AppConfig.Int("MaxLocker")
	if err != nil {
		resBad.Error = "please set max locker value on folder conf/app.conf"
		resBad.Code = "2"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		fmt.Println(resBad)
		return
	}
	if govalidator.IsNumeric(ob.BoardVersion) == false || govalidator.IsNumeric(ob.NoBoard) == false {
		resBad.Error = "param value must be digit as string"
		resBad.Code = "2"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		fmt.Println(resBad)
		return
	}
	noChannel, err1 := strconv.Atoi(ob.NoChannel)
	boardVersion, err2 := strconv.Atoi(ob.BoardVersion)
	noBoard, err3 := strconv.Atoi(ob.NoBoard)
	if err1 != nil || err2 != nil || err3 != nil {
		resBad.Error = err1.Error() + " ---------- " + err2.Error()
		resBad.Code = "2"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		fmt.Println(resBad)
		return
	}
	if noChannel == 0 {
		resBad.Error = "your set locker is more than available locker door or value locker must > 0"
		resBad.Code = "2"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		fmt.Println(resBad)
		return
	}

	if boardVersion == 0 {
		resBad.Error = "your set board must 2 or 3"
		resBad.Code = "3"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		fmt.Println(resBad)
		return
	}
	//engine code will be here
	var byteSendSerial []byte
	if boardVersion == 2 {
		//channel -1 (start dari 0x00)
		byteSendSerial = engine.CmdBoardRev2[noBoard-1].ProtocolPopbox[noChannel-1]
	} else if boardVersion == 3 {
		byteSendSerial = engine.CmdBoardRev3[noBoard-1].ProtocolPopbox[noChannel]
	} else {
		resBad.Error = "your set board must 2 or 3"
		resBad.Code = "3"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		fmt.Println(resBad)
		return
	}
	err = engine.OpenAndSendSerialPopbox(byteSendSerial)
	if err != nil {
		resBad.Error = err.Error()
		resBad.Code = "4"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		fmt.Println(resBad)
		return
	}
	//set response status to ok
	var resOk models.ResponseJSONOk
	var tempStr string
	for index := 0; index < len(byteSendSerial); index++ {
		tempStr = tempStr + ", " + fmt.Sprintf("0x%x", byteSendSerial[index])
	}
	fmt.Println(byteSendSerial)
	resOk.Message = "Success to open locker with byte send=" + tempStr + " and board rev:" + ob.BoardVersion + "board:" + ob.NoBoard + " channel:" + ob.NoChannel
	c.Data["json"] = &resOk
	c.Ctx.ResponseWriter.WriteHeader(http.StatusOK)
	c.ServeJSON()
	fmt.Println(resOk)
	return
}

// Propertiesboard ...
// @Title Command get setting board on conf/popbox.conf
// @Description   return "board_version" ,  "max_board_slot" , "max_channel": "string" , setting available on conf/popbox.conf
// @Param	body	body 	models.ParamBoardProperties	true		"param locker number and board version to open"
// @Success 200 {object} models.ResponBoardProperties  "succes open locker"
// @Failure 400 "{"Code": "1", "Error": "error message"} "
// @router /propertiesboard [post]
func (c *DeviceController) Propertiesboard() {
	var ob models.ParamBoardProperties
	var resBad models.ResponseJSONBad
	var resOk models.ResponBoardProperties
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &ob)
	if err != nil {
		resBad.Error = "param invalid"
		resBad.Code = "1"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		return
	}
	// maxLocker, err := beego.AppConfig.Int("MaxLocker")
	if err != nil {
		resBad.Error = "please set max locker value on folder conf/app.conf"
		resBad.Code = "2"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		return
	}
	if govalidator.IsNumeric(ob.BoardVersion) == false {
		resBad.Error = "param value must digit as string"
		resBad.Code = "3"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		return
	}
	boardVersion, _ := strconv.Atoi(ob.BoardVersion)
	if boardVersion < 2 && boardVersion > 3 {
		resBad.Error = "board version must 2 or 3"
		resBad.Code = "3"
		c.Data["json"] = &resBad
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.ServeJSON()
		return
	}
	if boardVersion == 2 {
		resOk.BoardVersion = ob.BoardVersion
		resOk.MaxBoardSlot = strconv.Itoa(engine.MaxBoardRev2)
		resOk.MaxChannel = strconv.Itoa(engine.MaxChannelRev2)
		c.Data["json"] = &resOk
		c.ServeJSON()
		return
	}
	if boardVersion == 3 {
		resOk.BoardVersion = ob.BoardVersion
		resOk.MaxBoardSlot = strconv.Itoa(engine.MaxBoardRev3)
		resOk.MaxChannel = strconv.Itoa(engine.MaxChannelRev3)
		c.Data["json"] = &resOk
		c.ServeJSON()
		return
	}
}

// @Title xxx
// @Description xxx
// @Param	body	body 	models.paramPickupDetailStruct	true		"xxx"
// @Success 200 {object} models.device.ResponseJSONOk  "xxx"
// @Failure 400 "{"Code": "1", "Error": "param invalid"} ,{"Code": "2", "Error": "please set max locker value on folder conf/app.conf"},{"Code": "3", "Error": "your set locker is more than available locker door or value locker must > 0"}"
// @router /randomopenlocker [get]
func (c *DeviceController) Randomopenlocker() {
	portSel := random(1, 12)
	paramByte := engine.CmdBoardRev2[0].ProtocolPopbox[portSel]
	engine.OpenAndSendSerialPopbox(paramByte)
	c.Data["json"] = map[string]string{"locker_number_open": strconv.Itoa(portSel)}
	c.ServeJSON()
}
func random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}
