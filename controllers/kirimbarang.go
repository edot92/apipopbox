package controllers

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/edot92/apipopbox/models"

	"github.com/astaxie/beego"
)

// KirimbarangController ....
type KirimbarangController struct {
	beego.Controller
}
type paramPickupDetailStruct struct {
	Token     string `json:"token"`
	Phone     string `json:"phone"`
	InvoiceID string `json:"invoice_id"`
}
type resPopsend struct {
	Response struct {
		Code    string `json:"code"`
		Message string `json:"message"`
	} `json:"response"`
	Data struct {
		InvoiceID string `json:"invoice_id"`
		Status    string `json:"status"`
	} `json:"data"`
}

type respPickupDetailStruct struct {
	Response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	} `json:"response"`
	TotalPage int `json:"total_page"`
	Data      []struct {
		InvoiceID              string `json:"invoice_id"`
		PickupAddress          string `json:"pickup_address"`
		PickupLockerName       string `json:"pickup_locker_name"`
		PickupLockerSize       string `json:"pickup_locker_size"`
		ItemDescription        string `json:"item_description"`
		RecipientName          string `json:"recipient_name"`
		RecipientPhone         string `json:"recipient_phone"`
		RecipientAddressDetail string `json:"recipient_address_detail"`
		RecipientAddress       string `json:"recipient_address"`
		RecipientLockerName    string `json:"recipient_locker_name"`
		RecipientLockerSize    string `json:"recipient_locker_size"`
		RecipientEmail         string `json:"recipient_email"`
		Status                 string `json:"status"`
		StatusHistory          string `json:"status_history"`
		TotalAmount            string `json:"total_amount"`
		OrderDate              string `json:"order_date"`
	} `json:"data"`
}

// @Title xxx
// @Description xxx
// @Param	body	body 	models.paramPickupDetailStruct	true		"xxx"
// @Success 200 {object} models.device.ResponseJSONOk  "xxx"
// @Failure 400 "{"Code": "1", "Error": "param invalid"} ,{"Code": "2", "Error": "please set max locker value on folder conf/app.conf"},{"Code": "3", "Error": "your set locker is more than available locker door or value locker must > 0"}"
// @router /pickupdetail [post]
func (c *KirimbarangController) Pickupdetail() {
	var tempParam paramPickupDetailStruct
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &tempParam)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
		return
	}
	tempParam.Token = "767ae89fec88a53dbb465adb4d0c76c294a8a28e"
	tempParam.Phone = " "
	// tempParam.InvoiceId = "PLA1706162226516"
	jsonValue, err := json.Marshal(&tempParam)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
		return
	}
	resp, err := http.Post("http://api-dev.popbox.asia/pickup/detail", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
		return
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
		return
	}
	var temprespPickupDetail respPickupDetailStruct
	err = json.Unmarshal(respBody, &temprespPickupDetail)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
		return
	}
	if len(temprespPickupDetail.Data) == 0 {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString("PLA Not Found")
		return
	}
	c.Data["json"] = map[string]interface{}{
		"data": temprespPickupDetail.Data[0],
	}
	// c.Data['json']=map[string]interface{}{
	// 	"data":temprespPickupDetail.Data,
	// }
	c.ServeJSON()
}

// Popsend ....
// @Title xxx
// @Description xxx
// @Param	body	body 	models.ParamPopsend	true		"xxx"
// @Success 200 {object} models.device.ResponseJSONOk  "xxx"
// @Failure 400 "{"Code": "1", "Error": "param invalid"} ,{"Code": "2", "Error": "please set max locker value on folder conf/app.conf"},{"Code": "3", "Error": "your set locker is more than available locker door or value locker must > 0"}"
// @router /popsend [post]
func (c *KirimbarangController) Popsend() {
	var tempParam models.ParamPopsend
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &tempParam)
	tempParam.Token = "767ae89fec88a53dbb465adb4d0c76c294a8a28e"
	tempParam.LockerName = "office est station"
	tempParam.LockerSize = "S"
	tempParam.LockerNumber = "1"
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
		return
	}
	// tempParam.InvoiceId = "PLA1706162226516"
	jsonValue, err := json.Marshal(&tempParam)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
	}

	resp, err := http.Post("http://api-dev.popbox.asia/projectx/popsend", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
		return
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
		return
	}

	var temprespPickupDetail resPopsend
	err = json.Unmarshal(respBody, &temprespPickupDetail)

	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusBadRequest)
		c.Ctx.WriteString(err.Error())
		return
	}

	c.Data["json"] = map[string]interface{}{
		"data": temprespPickupDetail.Data,
	}
	// c.Data['json']=map[string]interface{}{
	// 	"data":temprespPickupDetail.Data,
	// }
	c.ServeJSON()
}
