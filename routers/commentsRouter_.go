package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["gitlab.com/edot92/apipopbox/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/apipopbox/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Forceon",
			Router: `/forceon`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/apipopbox/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/apipopbox/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Propertiesboard",
			Router: `/propertiesboard`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

}
