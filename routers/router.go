// @APIVersion 1.0.0
// @Title API POPBOX
// @Description List API TO OPERATE POPBOX LOCKER
// @Contact edyprasetiyoo@gmail.com
// @TermsOfServiceUrl #
// @License xxxx
// @LicenseUrl #
package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
	"gitlab.com/edot92/apipopbox/controllers"
)

func init() {
	beego.InsertFilter("/*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Authorization", "authorization", "Access-Control-Allow-Origin", "Content-Type", "X-CSRF-TOKEN"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
		AllowOrigins:     []string{"https://127.0.0.1/:5003", "http://localhost:8081", "http://localhost:8888", "http://192.168.0.103:8080", "http://192.168.0.103:8081"},
	}))
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("/device",
			beego.NSInclude(
				&controllers.DeviceController{},
			),
		),
		beego.NSNamespace("/proses",
			beego.NSInclude(
				&controllers.KirimbarangController{},
			),
		),
	)

	beego.AddNamespace(ns)
}
