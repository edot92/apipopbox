package engine

import (
	"strconv"

	"github.com/astaxie/beego"
)

// dataStructBoard  protocol and revision board
type dataStructBoard struct {
	// BoardRev string `json:"board_rev"`
	BoardSlot      string         `json:"board_slot"`
	ProtocolPopbox map[int][]byte `json:"protocol_popbox"`
}

// CmdBoardRev2 CmdBoardRev2[board_slot].ProtocolPopbox[channel value to on]
var CmdBoardRev2 []dataStructBoard

// CmdBoardRev3 CmdBoardRev3[board_slot].ProtocolPopbox[channel value to on]
var CmdBoardRev3 []dataStructBoard
var MaxChannelRev2 int
var MaxBoardRev2 int
var MaxChannelRev3 int
var MaxBoardRev3 int

func init() {
	maxChannelRev2, _ := beego.AppConfig.Int("BoardRev2::MaxChannel")
	maxBoardRev2, _ := beego.AppConfig.Int("BoardRev2::MaxBoardSlot") //so 12*4 channel
	maxChannelRev3, _ := beego.AppConfig.Int("BoardRev3::MaxChannel")
	maxBoardRev3, _ := beego.AppConfig.Int("BoardRev3::MaxBoardSlot")

	MaxChannelRev2 = maxChannelRev2
	MaxBoardRev2 = maxBoardRev2 //so 12*4 channel
	MaxChannelRev3 = maxChannelRev3
	MaxBoardRev3 = maxBoardRev3
	initBoardRev2()
	initBoardRev3()
}
func initBoardRev2() {
	// PopboxSerial.ProtocolPopbox = make(map[int][]byte)
	//add protokol rev2
	//structur3
	//4 byte data = n1 n2 n3 n4
	//n1= start command
	//n2= fixed val 0x03
	//n3= channel board (max 12 channel on 1 board)
	//n4= unique key , 0x5B on channel 1 , 0x5A on channel 2 , 0x59 on channel 3 , 0xNN on channel X,NN is decrement from Channel 1
	//ex  A0 03 01 5B LOCKER 1 (BOARD1), force ON selenoid at channel 1 BOARD 1
	//ex  A0 03 01 5A LOCKER 1 (BOARD1), force ON selenoid at channel 1 BOARD 2
	//ex  A1 03 01 5A LOCKER 1 (BOARD2), force ON selenoid at channel 1 BOARD 1
	//ex  A1 03 02 59 LOCKER 2 (BOARD2), force ON selenoid at channel 1 BOARD 2
	// addrChannel := 0x00
	// endDecrement := 0x5E
	addrBoardRev2 := 0x5B

	for index := 0; index < MaxBoardRev2; index++ {
		tempUniqueKey := addrBoardRev2
		tempProtocol := make(map[int][]byte)
		for i := 0; i < MaxChannelRev2; i++ {
			tempProtocol[i] = []byte{
				0xA0 + byte(index), // board slot
				0x03,
				byte(i + 1),         //this is channel address
				byte(tempUniqueKey), //and this is unique key, always decrement
			}
			tempUniqueKey = tempUniqueKey - 1
		}
		CmdBoardRev2 = append(CmdBoardRev2, dataStructBoard{
			BoardSlot:      strconv.Itoa(index),
			ProtocolPopbox: tempProtocol,
		})
		addrBoardRev2 = addrBoardRev2 - 1 //decrement unique key for other board
	}
}
func initBoardRev3() {
	//add protokol rev3
	addrChannel := 0x00
	endDecrement := 0x5E

	tempProtocol := make(map[int][]byte)
	for index := 0; index < MaxBoardRev3; index++ {
		for i := 0; i < MaxChannelRev3; i++ {
			tempProtocol[i] = []byte{
				0xA0,
				byte(addrChannel),
				0x01,
				0x00,
				0x00,
				0x00,
				byte(endDecrement),
				0x00,
			}
			addrChannel = addrChannel + 1
			endDecrement = endDecrement - 1
		}
		CmdBoardRev3 = append(CmdBoardRev3, dataStructBoard{
			BoardSlot:      strconv.Itoa(index),
			ProtocolPopbox: tempProtocol,
		})
	}
}

/*********old method **************************/
/*package engine

// PopboxSerialStruct  protocol and revision board
// BoardRev board revision value 2 or 3
// ProtocolPopbox array of value command base on index channel
type PopboxSerialStruct struct {
	BoardRev       string         `json:"board_rev"`
	ProtocolPopbox map[int][]byte `json:"protocol_popbox"`
}

// PopboxSerial protocol data with rev board
var PopboxSerial []PopboxSerialStruct

func init() {

	// PopboxSerial.ProtocolPopbox = make(map[int][]byte)
	//add protokol rev2
	//structur3
	//4 byte data = n1 n2 n3 n4
	//n1= start command
	//n2= fixed val 0x03
	//n3= channel board (max 12 channel on 1 board)
	//n4= unique key , 0x5B on channel 1 , 0x5A on channel 2 , 0x59 on channel 3 , 0xNN on channel X,NN is decrement from Channel 1
	//ex  A0 03 01 5B LOCKER 1 (BOARD1), force ON selenoid at channel 1 BOARD 1
	//ex  A0 03 01 5A LOCKER 1 (BOARD2), force ON selenoid at channel 1 BOARD 2
	addrChannel := 0x00
	endDecrement := 0x5E
	addrBoardRev2 := 0x5B
	tempProtocol := make(map[int][]byte)
	maxChannelRev2 := 12
	maxBoardRev2 := 4 //so 12*4 channel
	for index := 0; index < maxBoardRev2; index++ {
		tempUniqueKey := addrBoardRev2
		for i := 0; i < maxChannelRev2; i++ {
			tempProtocol[i] = []byte{
				0xA0,
				0x03,
				byte(i + 1),         //this is channel address
				byte(tempUniqueKey), //and this is unique key, always decrement
			}
			tempUniqueKey = tempUniqueKey - 1
		}
		PopboxSerial = append(PopboxSerial, PopboxSerialStruct{
			BoardRev:       "2",
			ProtocolPopbox: tempProtocol,
		})
		addrBoardRev2 = addrBoardRev2 - 1 //decrement unique key for other board
	}
	//add protokol rev3
	addrChannel = 0x00
	endDecrement = 0x5E
	tempProtocol2 := make(map[int][]byte) //init again
	for i := 0; i < 24; i++ {
		tempProtocol2[i] = []byte{
			0xA0,
			byte(addrChannel),
			0x01,
			0x00,
			0x00,
			0x00,
			byte(endDecrement),
			0x00,
		}
		addrChannel = addrChannel + 1
		endDecrement = endDecrement - 1
	}
	PopboxSerial = append(PopboxSerial, PopboxSerialStruct{
		BoardRev:       "3",
		ProtocolPopbox: tempProtocol,
	})

}
*/
