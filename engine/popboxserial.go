package engine

import (
	"errors"
	"fmt"
	"log"

	"github.com/astaxie/beego"
	serial "go.bug.st/serial.v1"
)

// ComPort com port choice
var ComPort string
var baudrate int

func init() {
	var err error
	ComPort = beego.AppConfig.String("ComPort")
	baudrate, _ = beego.AppConfig.Int("Baudrate")
	if err != nil {
		log.Fatal("baudrate must be set on conf/app.conf ")
	}
}

// OpenAndSendSerialPopbox send serial command protocol popbox
func OpenAndSendSerialPopbox(paramSend []byte) error {
	ports, err := serial.GetPortsList()
	if err != nil {
		log.Fatal(err)
	}
	if len(ports) == 0 {
		return errors.New("No serial Ports Found")
	}

	// Print the list of detected ports
	for _, port := range ports {
		fmt.Printf("Found port: %v\n", port)
	}

	// Open the first serial port detected at 9600bps N81
	mode := &serial.Mode{
		BaudRate: baudrate,
		// Parity:   serial.NoParity,
		// DataBits: 8,
		// StopBits: serial.OneStopBit,
	}
	port, err := serial.Open(ComPort, mode)
	if err != nil {
		return err
	}
	defer port.Close()
	_, err = port.Write(paramSend)
	if err != nil {
		return err
	}
	fmt.Println("-------------------")
	for index := 0; index < len(paramSend); index++ {
		fmt.Printf("0x%x ", paramSend[index])
	}
	fmt.Println("-------------------")
	return nil
}
