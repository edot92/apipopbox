package engine

import (
	"strconv"
	"testing"
)

func TestInitBoardRev2(t *testing.T) {
	const testMaxChannel int = 2
	const testMaxBoard int = 2
	const testQtyBytePerSend int = 4
	resultsTestRev2 := []dataStructBoard{
		{ //board1
			"2",
			map[int][]byte{
				0: { //loker1 board 1
					0xA0,
					0x03,
					0x01,
					0x5B,
				},
				1: { //loker2 board 1
					0xA0,
					0x03,
					0x02,
					0x5A,
				},
			},
		},
		{ //board2
			"2",
			map[int][]byte{
				0: { //loker1 board 1
					0xA0,
					0x03,
					0x01,
					0x5A,
				},
				1: { //loker2 board 1
					0xA0,
					0x03,
					0x02,
					0x59,
				},
			},
		},
	}
	addrBoardRev2 := 0x5B
	tempProtocol := make(map[int][]byte)
	maxChannelRev2 := testMaxChannel //default 12
	maxBoardRev2 := testMaxBoard
	for index := 0; index < maxBoardRev2; index++ {
		tempUniqueKey := addrBoardRev2
		for i := 0; i < maxChannelRev2; i++ {
			tempProtocol[i] = []byte{
				0xA0,
				0x03,
				byte(i + 1),         //this is channel address
				byte(tempUniqueKey), //and this is unique key, always decrement
			}

			//testing here
			for xByte := 0; xByte < testQtyBytePerSend; xByte++ { //4 is total byte on 1 commnad
				if tempProtocol[i][xByte] != resultsTestRev2[index].ProtocolPopbox[i][xByte] {
					t.Errorf(": return 0x%x want 0x%x on board: %d , channel %d ,on byte %d from 4 ", tempProtocol[i][xByte], resultsTestRev2[index].ProtocolPopbox[i][xByte], index, i, xByte)
				}
			}

			//end testing
			tempUniqueKey = tempUniqueKey - 1
			tempByte := tempProtocol[i]
			err := OpenAndSendSerialPopbox(tempByte)
			if err != nil {
				t.Errorf(err.Error())
			} else {
				t.Log("No err")
			}
		}
		CmdBoardRev2 = append(CmdBoardRev2, dataStructBoard{
			BoardSlot:      strconv.Itoa(index),
			ProtocolPopbox: tempProtocol,
		})
		addrBoardRev2 = addrBoardRev2 - 1 //decrement unique key for other board

	}

}

// func TestInitBoardRev3(t *testing.T) {
// 	const testMaxChannel int = 2
// 	const testMaxBoard int = 1
// 	const testQtyBytePerSend int = 8
// 	resultsTestRev3 := []dataStructBoard{
// 		{ //board1
// 			"3",
// 			map[int][]byte{
// 				0: { //loker1 board 1
// 					0xA0,
// 					0x00, //board 1 = 0x00
// 					0x01,
// 					0x00,
// 					0x00,
// 					0x00,
// 					0x5E, //unique key, next increment
// 					0x00,
// 				},
// 				1: { //loker2 board 1
// 					0xA0,
// 					0x01, //board 2 =0x01
// 					0x01,
// 					0x00,
// 					0x00,
// 					0x00,
// 					0x5D, //unique key,
// 					0x00,
// 				},
// 			},
// 		},
// 	}
// 	addrChannel := 0x00
// 	endDecrement := 0x5E
// 	maxBoardRev3 := testMaxBoard
// 	tempProtocol2 := make(map[int][]byte) //init again
// 	for index := 0; index < maxBoardRev3; index++ {
// 		for i := 0; i < testMaxChannel; i++ { //default 1 board 24 channel
// 			tempProtocol2[i] = []byte{
// 				0xA0,
// 				byte(addrChannel),
// 				0x01,
// 				0x00,
// 				0x00,
// 				0x00,
// 				byte(endDecrement),
// 				0x00,
// 			}

// 			addrChannel = addrChannel + 1
// 			endDecrement = endDecrement - 1
// 			for xByte := 0; xByte < testQtyBytePerSend; xByte++ { //testQtyBytePerSend is total byte on 1 commnad
// 				if tempProtocol2[i][xByte] != resultsTestRev3[index].ProtocolPopbox[i][xByte] {
// 					t.Errorf(": return 0x%x want 0x%x on board: %d , channel %d ,on byte %d from 4 ", tempProtocol2[i][xByte], resultsTestRev3[index].ProtocolPopbox[i][xByte], index, i, xByte)
// 				}
// 			}
// 		}
// 		CmdBoardRev3 = append(CmdBoardRev3, dataStructBoard{
// 			BoardSlot:      strconv.Itoa(index),
// 			ProtocolPopbox: tempProtocol2,
// 		})
// 	}
// }
