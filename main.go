package main

import (
	"github.com/astaxie/beego"
	_ "gitlab.com/edot92/apipopbox/routers"
)

func main() {
	// for j := 0; j < 2; j++ {
	// 	for i := 0; i < 12; i++ {
	// 		byteTemp := engine.CmdBoardRev2[j].ProtocolPopbox[i]
	// 		err := engine.OpenAndSendSerialPopbox(byteTemp)
	// 		if err != nil {
	// 			log.Fatal(err)
	// 		}
	// 		time.Sleep(500 * time.Millisecond)
	// 	}
	// }

	// if beego.BConfig.RunMode == "dev" {
	beego.BConfig.WebConfig.DirectoryIndex = true
	beego.BConfig.WebConfig.EnableDocs = true
	beego.BConfig.WebConfig.StaticDir["/docs"] = "swagger"
	beego.BConfig.CopyRequestBody = true
	beego.BConfig.WebConfig.TemplateLeft = "<%"
	beego.BConfig.WebConfig.TemplateRight = "%>"
	// }

	beego.SetStaticPath("/", "webui")

	beego.SetStaticPath("static", "webui/static")
	beego.SetStaticPath("staticpublic/static", "webui/static")

	beego.Run()
}
